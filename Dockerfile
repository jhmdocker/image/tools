ARG UBUNTU_VERSION=ND
FROM registry.gitlab.com/jhmdocker/image/ubuntu:${UBUNTU_VERSION}

RUN apt-get update && apt-get install -y \
    dnsutils iputils-ping tcpdump wget curl vim telnet traceroute net-tools dnsdiag nmap git jq sudo rsync ssh

# Novas versões em
# https://github.com/natesales/q/releases
RUN wget -O q.deb https://github.com/natesales/q/releases/download/v0.19.2/q_0.19.2_linux_amd64.deb && \
    apt install -y ./q.deb && \
    rm -fr q.deb
