UBUNTU_VERSION=24.04
VERSION=latest
IMG=registry.gitlab.com/jhmdocker/image/tools

build:
	docker build --pull -t $(IMG) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
